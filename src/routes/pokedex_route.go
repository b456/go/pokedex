package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/b456/go/pokedex/src/config"
	"gitlab.com/b456/go/pokedex/src/controllers"
	"gitlab.com/b456/go/pokedex/src/middleware"
	"gitlab.com/b456/go/pokedex/src/repository"
	"gitlab.com/b456/go/pokedex/src/service"
)

func PokedexRoutes(route *gin.Engine) {
	db := config.SetupDatabaseConnection()
	pokemonRepo := repository.NewPokemonRepository(db)
	abilityRepo := repository.NewAbilityRepository(db)
	pokemonAbilityRepo := repository.NewPokemonAbilityRepository(db)

	p := service.NewPokemonService(pokemonRepo)
	a := service.NewAbilityService(abilityRepo)
	pa := service.NewPokemonAbilityService(pokemonAbilityRepo)

	PokedexCtrl := controllers.NewAllPokedexCtrl(p, a, pa)
	// Input timestamp validation using middleware
	route.Use(middleware.Timestamp())
	route.GET("/find", PokedexCtrl.FindPokemonAbility)
	route.GET("/pokemon", PokedexCtrl.FindPokemon)
}
