package repository

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/b456/go/pokedex/src/entity"
)

type AbilityRepoMock struct {
	Mock mock.Mock
}

func (repo *AbilityRepoMock) FindAbilityByName(name string) entity.Ability {
	args := repo.Mock.Called(name)
	if args.Get(0) == nil {
		return entity.Ability{}
	} else {
		ability := args.Get(0).(entity.Ability)
		return ability
	}
}
func (repo *AbilityRepoMock) FindAbilityByID(abilityID uint64) entity.Ability {
	args := repo.Mock.Called(abilityID)
	if args.Get(0) == nil {
		return entity.Ability{}
	} else {
		ability := args.Get(0).(entity.Ability)
		return ability
	}
}
func (repo *AbilityRepoMock) FindAbilityByPokemon(abilityID []uint64) []entity.Ability {
	args := repo.Mock.Called(abilityID)
	if args.Get(0) == nil {
		return []entity.Ability{}
	} else {
		ability := args.Get(0).([]entity.Ability)
		return ability
	}
}
