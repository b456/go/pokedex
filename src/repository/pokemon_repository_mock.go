package repository

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/b456/go/pokedex/src/entity"
)

type PokemonRepoMock struct {
	Mock mock.Mock
}

func (repo *PokemonRepoMock) FindPokemonByName(name string) entity.Pokemon {
	args := repo.Mock.Called(name)
	if args.Get(0) == nil {
		return entity.Pokemon{}
	} else {
		pokemon := args.Get(0).(entity.Pokemon)
		return pokemon
	}
}
