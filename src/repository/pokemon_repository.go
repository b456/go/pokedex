package repository

import (
	"gitlab.com/b456/go/pokedex/src/entity"
	"gorm.io/gorm"
)

type PokemonRepository interface {
	FindPokemonByName(name string) entity.Pokemon
}

type pokemonConnection struct {
	connection *gorm.DB
}

func (db *pokemonConnection) FindPokemonByName(PokemonName string) entity.Pokemon {
	var pokemon entity.Pokemon
	db.connection.Where("name LIKE ?", PokemonName).Find(&pokemon)
	return pokemon
}

func NewPokemonRepository(dbConn *gorm.DB) PokemonRepository {
	return &pokemonConnection{
		connection: dbConn,
	}
}
