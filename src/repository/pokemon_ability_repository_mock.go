package repository

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/b456/go/pokedex/src/entity"
)

type PokemonAbilityRepoMock struct {
	Mock mock.Mock
}

func (repo *PokemonAbilityRepoMock) FindPokemonAbilityByPokemonID(pokemonID uint64) []entity.PokemonAbility {
	args := repo.Mock.Called(pokemonID)
	if args.Get(0) == nil {
		return []entity.PokemonAbility{}
	} else {
		pokemonAbility := args.Get(0).([]entity.PokemonAbility)
		return pokemonAbility
	}
}
func (repo *PokemonAbilityRepoMock) FindPokemonAbilityByAbilityID(abilityID uint64) []entity.PokemonAbility {
	args := repo.Mock.Called(abilityID)
	if args.Get(0) == nil {
		return []entity.PokemonAbility{}
	} else {
		pokemonAbility := args.Get(0).([]entity.PokemonAbility)
		return pokemonAbility
	}
}
