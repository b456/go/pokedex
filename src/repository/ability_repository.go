package repository

import (
	"gitlab.com/b456/go/pokedex/src/entity"
	"gorm.io/gorm"
)

type AbilityRepository interface {
	FindAbilityByName(abilityName string) entity.Ability
	FindAbilityByID(abilityID uint64) entity.Ability
	FindAbilityByPokemon(abilityID []uint64) []entity.Ability
}

type abilityConnection struct {
	connection *gorm.DB
}

func NewAbilityRepository(dbConn *gorm.DB) AbilityRepository {
	return &abilityConnection{
		connection: dbConn,
	}
}

func (db *abilityConnection) FindAbilityByName(abilityName string) entity.Ability {
	var ability entity.Ability
	db.connection.Where("name LIKE ?", abilityName).Find(&ability)
	return ability
}

func (db *abilityConnection) FindAbilityByPokemon(abilityID []uint64) []entity.Ability {
	var ability []entity.Ability
	db.connection.Find(&ability, abilityID)
	return ability
}
func (db *abilityConnection) FindAbilityByID(abilityID uint64) entity.Ability {
	var ability entity.Ability
	db.connection.Find(&ability, abilityID)
	return ability
}
