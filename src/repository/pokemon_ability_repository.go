package repository

import (
	"gitlab.com/b456/go/pokedex/src/entity"
	"gorm.io/gorm"
)

type PokemonAbilityRepository interface {
	FindPokemonAbilityByPokemonID(pokemonID uint64) []entity.PokemonAbility
	FindPokemonAbilityByAbilityID(abilityID uint64) []entity.PokemonAbility
}

type pokemonAbilityConnection struct {
	connection *gorm.DB
}

func (db *pokemonAbilityConnection) FindPokemonAbilityByPokemonID(pokemonID uint64) []entity.PokemonAbility {
	var pokemonAbility []entity.PokemonAbility
	db.connection.Where("pokemon_id", pokemonID).Find(&pokemonAbility)
	return pokemonAbility
}
func (db *pokemonAbilityConnection) FindPokemonAbilityByAbilityID(abilityID uint64) []entity.PokemonAbility {
	var pokemonAbility []entity.PokemonAbility
	db.connection.Where("ability_id", abilityID).Find(&pokemonAbility)
	return pokemonAbility
}

func NewPokemonAbilityRepository(dbConn *gorm.DB) PokemonAbilityRepository {
	return &pokemonAbilityConnection{
		connection: dbConn,
	}
}
