package middleware

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Error struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func Timestamp() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		reqTimestamp := ctx.Request.Header.Get("Timestamp")
		if reqTimestamp == "" {
			ctx.JSON(http.StatusBadRequest, Error{
				Status:  "failed",
				Message: "Header Timestamp not found",
			})
			ctx.Abort()
			return
		}
		YYYYMMDDHHMMSS := "20060102150405"
		formatTimestamp, _ := time.ParseInLocation(YYYYMMDDHHMMSS, reqTimestamp, time.Local)
		timeNow := time.Now()
		timeAllow := timeNow.Add(-60 * time.Second)
		fmt.Println(timeAllow)
		fmt.Println(timeNow)
		fmt.Println(reqTimestamp)
		fmt.Println(formatTimestamp)
		timeValidation := timeAllow.Before(formatTimestamp)
		if !timeValidation {
			ctx.JSON(http.StatusBadRequest, Error{
				Status:  "failed",
				Message: "Timestamp not allow",
			})
			ctx.Abort()
			return
		}
		ctx.Next()
	}
}
