package test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/b456/go/pokedex/src/controllers"
	"gitlab.com/b456/go/pokedex/src/dto"
	"gitlab.com/b456/go/pokedex/src/repository"
	"gitlab.com/b456/go/pokedex/src/service"
)

var (
	// router
	r = SetUpRouter()
	// repository mock
	pokemonRepo        = &repository.PokemonRepoMock{Mock: mock.Mock{}}
	abilityRepo        = &repository.AbilityRepoMock{Mock: mock.Mock{}}
	pokemonAbilityRepo = &repository.PokemonAbilityRepoMock{Mock: mock.Mock{}}
	// service mock
	pokemonService        = service.NewPokemonService(pokemonRepo)
	abilityService        = service.NewAbilityService(abilityRepo)
	pokemonAbilityService = service.NewPokemonAbilityService(pokemonAbilityRepo)
	// controllers
	PokedexCtrl = controllers.NewAllPokedexCtrl(pokemonService, abilityService, pokemonAbilityService)
)

func SetUpRouter() *gin.Engine {
	router := gin.Default()
	return router
}

// func TestFindPokemonAbilityFoundPokemon(t *testing.T) {

// 	// inputName := "pidgey"
// 	parm := url.Values{}
// 	parm.Add("Q", "pidgey")
// 	// params := strings.NewReader(parm.Encode())
// 	outputPokemon := entity.Pokemon{
// 		ID:        1,
// 		Name:      "pidgey",
// 		Types:     "normal,flying",
// 		CreatedAt: time.Time{},
// 		UpdatedAt: time.Time{},
// 	}
// 	pokemonRepo.Mock.On("FindPokemonByName", "pidgey").Return(outputPokemon)
// 	r.GET("/find", PokedexCtrl.FindPokemonAbility)
// 	req, _ := http.NewRequest("GET", "/find", strings.NewReader(parm.Encode()))
// 	req.URL.Query()

// 	w := httptest.NewRecorder()
// 	r.ServeHTTP(w, req)
// 	fmt.Println(w.Body)
// 	bodyBytes, _ := ioutil.ReadAll(w.Body)
// 	bodyString := string(bodyBytes)
// 	var resMsgReal dto.ResFailed
// 	json.Unmarshal([]byte(bodyString), &resMsgReal)
// 	// fmt.Println(resMsgReal)
// 	assert.Equal(t, 200, w.Code)
// }

func TestFindPokemonAbilityWithoutQ(t *testing.T) {
	r.GET("/find", PokedexCtrl.FindPokemonAbility)
	req, _ := http.NewRequest("GET", "/find", nil)
	resMsgMockPA := "Key: 'ReqPokemonAbility.Q' Error:Field validation for 'Q' failed on the 'required' tag"

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	bodyBytes, _ := ioutil.ReadAll(w.Body)
	bodyString := string(bodyBytes)
	var resMsgReal dto.ResFailed
	json.Unmarshal([]byte(bodyString), &resMsgReal)
	assert.Equal(t, resMsgMockPA, resMsgReal.Message)
}

func TestFindPokemonWithoutQ(t *testing.T) {
	r.GET("/pokemon", PokedexCtrl.FindPokemon)
	req, _ := http.NewRequest("GET", "/pokemon", nil)
	resMsgMockP := "Key: 'ReqPokemon.Q' Error:Field validation for 'Q' failed on the 'required' tag"

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	bodyBytes, _ := ioutil.ReadAll(w.Body)
	bodyString := string(bodyBytes)
	var resMsgReal dto.ResFailed
	json.Unmarshal([]byte(bodyString), &resMsgReal)
	assert.Equal(t, resMsgMockP, resMsgReal.Message)
}
