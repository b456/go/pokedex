package entity

import (
	"time"
)

type Pokemon struct {
	ID        uint64 `gorm:"size:20;not null;uniqueIndex;primary_key"`
	Name      string `gorm:"size:100"`
	Types     string `gorm:"size:100"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
