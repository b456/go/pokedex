package entity

import "time"

type Ability struct {
	ID          uint64 `gorm:"size:20;not null;uniqueIndex;primary_key"`
	Name        string `gorm:"size:100"`
	Description string `gorm:"size:255"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
