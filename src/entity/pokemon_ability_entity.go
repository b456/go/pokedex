package entity

import "time"

type PokemonAbility struct {
	ID        uint64  `gorm:"size:20;not null;uniqueIndex;primary_key"`
	PokemonID uint64  `gorm:"size:20;"`
	Pokemon   Pokemon `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	AbilityID uint64  `gorm:"size:20;"`
	Ability   Ability `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
