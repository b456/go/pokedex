package controllers

import (
	"net/http"
	"regexp"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/b456/go/pokedex/src/dto"
	"gitlab.com/b456/go/pokedex/src/service"
)

var validate = validator.New()

type AllPokedexCtrl interface {
	FindPokemonAbility(ctx *gin.Context)
	FindPokemon(ctx *gin.Context)
}

type allPokedexService struct {
	pokemonService        service.PokemonService
	abilityService        service.AbilityService
	pokemonAbilityService service.PokemonAbilityService
}

//Find Pokemon or Ability
// @Summary 		FindPokemonAbility
// @Descrition		Find Pokemon or Ability
// @Tags			Pokedex
// @Accept			mpfd
// @Param q body dto.ReqPokemonAbility true "name"
// @Produce			json
// @Router			/find [get]
func (s allPokedexService) FindPokemonAbility(ctx *gin.Context) {
	// ctx.JSON(http.StatusOK, "TEST")
	// return
	// Init variable
	var req dto.ReqPokemonAbility
	var resP dto.ResPokemon
	var resA dto.ResAbility
	var resFailed dto.ResFailed
	resFailed.Status = "failed"

	// Validation input data
	req.Q = ctx.Query("q")
	if errVal := validate.Struct(req); errVal != nil {
		resFailed.Message = errVal.Error()
		ctx.JSON(http.StatusBadRequest, resFailed)
		return
	}
	// Filter input with regex
	var regex, _ = regexp.Compile(`[-]`)
	req.Q = regex.ReplaceAllString(req.Q, " ")

	foundPekemon := s.pokemonService.FindByName(req.Q)
	if foundPekemon.Name == "" {
		foundAbility := s.abilityService.FindByName(req.Q)
		if foundAbility.Name == "" {
			resFailed.Message = "not found"
			ctx.JSON(http.StatusBadRequest, resFailed)
		} else {
			resA.Status = "success"
			resA.Message = "found ability"
			resA.Name = foundAbility.Name
			resA.Description = foundAbility.Description
			ctx.JSON(http.StatusOK, resA)
		}

	} else {
		foundPokemonAbility := s.pokemonAbilityService.FindByPokemonID(foundPekemon.ID)
		for i := range foundPokemonAbility {
			ability := s.abilityService.FindByID(foundPokemonAbility[i].AbilityID)
			resP.Data.Ability = append(resP.Data.Ability, ability.Name+" ("+ability.Description+")")
		}
		resP.Status = "success"
		resP.Message = "found pokemon"
		resP.Data.Name = foundPekemon.Name
		resP.Data.Type = foundPekemon.Types
		ctx.JSON(http.StatusOK, resP)
	}
}

//Find Pokemon
// @Summary 		FindPokemon
// @Descrition		Find Pokemon
// @Tags			Pokedex
// @Accept			mpfd
// @Param q body dto.ReqPokemon true "name"
// @Produce			json
// @Router			/pokemon [get]
func (s allPokedexService) FindPokemon(ctx *gin.Context) {
	// Init variable
	var req dto.ReqPokemon
	var resP dto.ResPokemon
	var resFailed dto.ResFailed
	resFailed.Status = "failed"

	// Validation input data
	req.Q = ctx.Query("q")
	if errVal := validate.Struct(req); errVal != nil {
		resFailed.Message = errVal.Error()
		ctx.JSON(http.StatusBadRequest, resFailed)
		return
	}
	// Filter input with regex
	var regex, _ = regexp.Compile(`[-]`)
	req.Q = regex.ReplaceAllString(req.Q, " ")

	foundPekemon := s.pokemonService.FindByName(req.Q)
	if foundPekemon.Name == "" {
		resFailed.Message = "not found"
		ctx.JSON(http.StatusBadRequest, resFailed)
		return
	} else {
		foundPokemonAbility := s.pokemonAbilityService.FindByPokemonID(foundPekemon.ID)
		for i := range foundPokemonAbility {
			ability := s.abilityService.FindByID(foundPokemonAbility[i].AbilityID)
			resP.Data.Ability = append(resP.Data.Ability, ability.Name+" ("+ability.Description+")")
		}
		resP.Status = "success"
		resP.Message = "found pokemon"
		resP.Data.Name = foundPekemon.Name
		resP.Data.Type = foundPekemon.Types
		ctx.JSON(http.StatusOK, resP)
	}
}

func NewAllPokedexCtrl(p service.PokemonService, a service.AbilityService, pa service.PokemonAbilityService) AllPokedexCtrl {
	return allPokedexService{
		pokemonService:        p,
		abilityService:        a,
		pokemonAbilityService: pa,
	}
}
