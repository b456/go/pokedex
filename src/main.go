package main

import (
	"time"

	"github.com/gin-gonic/gin"
	// "github.com/jinzhu/gorm"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/b456/go/pokedex/src/config"
	"gitlab.com/b456/go/pokedex/src/docs"
	"gitlab.com/b456/go/pokedex/src/routes"
	"gorm.io/gorm"
)

var (
	db *gorm.DB = config.SetupDatabaseConnection()
)

// @title POKEDEX APIs
// @version 1.0
// @description Immplement Test Code Backend.
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io
// @securityDefinitions.timestamp
// @in header
// @name timestamp
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:9000
// @BasePath /

func main() {

	loc, _ := time.LoadLocation("Asia/Jakarta")
	time.Local = loc

	defer config.CloseDatabaseConnection(db)
	port := config.GetEnv("APP_PORT", "9000")

	// router := gin.New()

	router := gin.Default()

	docs.SwaggerInfo.BasePath = "/"
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	routes.PokedexRoutes(router)
	router.Run(":" + port)

}
