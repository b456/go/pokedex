package config

import (
	"time"

	"gitlab.com/b456/go/pokedex/src/entity"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

//SetupDatabaseConnection is creating a new connection to our database
func SetupDatabaseConnection() *gorm.DB {
	loc, _ := time.LoadLocation("Asia/Jakarta")
	time.Local = loc

	// dbUser := GetEnv("DB_USER", "root")
	// dbPass := GetEnv("DB_PASSWORD", "password")
	// dbHost := GetEnv("DB_HOST", "localhost")
	// dbName := GetEnv("DB_NAME", "crawler_kasirpintar")
	// dbPort := GetEnv("DB_PORT", "3306")

	// dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", dbUser, dbPass, dbHost, dbPort, dbName)
	db, err := gorm.Open(sqlite.Open("database/pokedex.db"), &gorm.Config{})
	// db, err := gorm.Open("sqlite3", "pokedex.db")
	if err != nil {
		panic("Failed to create a connection to database")
	}
	// --- migrate ---
	db.AutoMigrate(&entity.Pokemon{}, &entity.Ability{}, &entity.PokemonAbility{})
	return db
}

//CloseDatabaseConnection method is closing a connection between your app and your db
func CloseDatabaseConnection(db *gorm.DB) {
	dbSQL, err := db.DB()
	if err != nil {
		panic("Failed to close connection from database")
	}
	dbSQL.Close()
}
