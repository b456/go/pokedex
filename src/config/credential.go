package config

import (
	"os"

	"github.com/joho/godotenv"
)

func GetEnv(key, fallback string) string {
	err := godotenv.Load(".env")
	if err != nil {
		return fallback
	}
	return os.Getenv(key)
}
