package dto

type ResFailed struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type ResPokemon struct {
	Status  string
	Message string
	Data    ResPokemonAbility
}

type ResPokemonAbility struct {
	Name    string
	Type    string
	Ability []string
}

type ResAbility struct {
	Status      string
	Message     string
	Name        string
	Description string
}
