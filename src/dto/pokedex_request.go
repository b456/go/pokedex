package dto

type ReqPokemonAbility struct {
	Q string `json:"q" validate:"required,max=100"`
}

type ReqPokemon struct {
	Q string `json:"q" validate:"required,max=50"`
}
