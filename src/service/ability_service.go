package service

import (
	"gitlab.com/b456/go/pokedex/src/entity"
	"gitlab.com/b456/go/pokedex/src/repository"
)

type AbilityService interface {
	FindByName(name string) entity.Ability
	FindByPokemon(abilityID []uint64) []entity.Ability
	FindByID(abilityID uint64) entity.Ability
}

type AbilityServiceRepo struct {
	AbilityServiceRepo repository.AbilityRepository
}

func (service AbilityServiceRepo) FindByName(name string) entity.Ability {
	return service.AbilityServiceRepo.FindAbilityByName(name)
}

func (service AbilityServiceRepo) FindByPokemon(abilityID []uint64) []entity.Ability {
	return service.AbilityServiceRepo.FindAbilityByPokemon(abilityID)
}
func (service AbilityServiceRepo) FindByID(abilityID uint64) entity.Ability {
	return service.AbilityServiceRepo.FindAbilityByID(abilityID)
}

func NewAbilityService(abilityRepo repository.AbilityRepository) AbilityService {
	return &AbilityServiceRepo{
		AbilityServiceRepo: abilityRepo,
	}
}
