package service

import (
	"gitlab.com/b456/go/pokedex/src/entity"
	"gitlab.com/b456/go/pokedex/src/repository"
)

type PokemonAbilityServiceRepo struct {
	PokemonAbilityServiceRepo repository.PokemonAbilityRepository
}

type PokemonAbilityService interface {
	FindByPokemonID(pokemonID uint64) []entity.PokemonAbility
	FindByAbilityID(abilityID uint64) []entity.PokemonAbility
}

func (service PokemonAbilityServiceRepo) FindByAbilityID(abilityID uint64) []entity.PokemonAbility {
	return service.PokemonAbilityServiceRepo.FindPokemonAbilityByAbilityID(abilityID)
}

func (service PokemonAbilityServiceRepo) FindByPokemonID(pokemonID uint64) []entity.PokemonAbility {
	return service.PokemonAbilityServiceRepo.FindPokemonAbilityByPokemonID(pokemonID)
}

func NewPokemonAbilityService(pokemonAbilityRepo repository.PokemonAbilityRepository) PokemonAbilityService {
	return &PokemonAbilityServiceRepo{
		PokemonAbilityServiceRepo: pokemonAbilityRepo,
	}
}
