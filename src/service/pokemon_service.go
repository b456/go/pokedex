package service

import (
	"gitlab.com/b456/go/pokedex/src/entity"
	"gitlab.com/b456/go/pokedex/src/repository"
)

type PokemonService interface {
	FindByName(name string) entity.Pokemon
}

type PokemonServiceRepo struct {
	PokemonServiceRepo repository.PokemonRepository
}

func (service PokemonServiceRepo) FindByName(name string) entity.Pokemon {
	return service.PokemonServiceRepo.FindPokemonByName(name)
}

func NewPokemonService(pokemonRepo repository.PokemonRepository) PokemonService {
	return &PokemonServiceRepo{
		PokemonServiceRepo: pokemonRepo,
	}
}
