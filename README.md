# POKEDEX
## Installation

- clone the project using https

```
git clone https://gitlab.com/b456/go/pokedex.git
```
- install dependencies

```
go install
```

## Run project (serve)

- Open project (project_name/src)
- Run project (main.go)
```
go run main.go
```

## Run test
- Open test location (project_name/src/test)
- Run test
```
go test
```
## Simple Doc APIs (swagger)
- After Run project (main.go)
```
go run main.go
```
- Open Swagger UI Doc using browser
```
http://127.0.0.1:9000/swagger/index.html
```
## Postman
- Collection
```
go test
```
- Change input / request data depent use case scenario
- Input data header Timestamp format YYYYMMDDHHmmSS